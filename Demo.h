#include <iostream>
#include <string>

using namespace std;

class OnlineVid
 {
    private:  //properties
         unsigned  views;
         string id; 
         


    public:  //methods
          OnlineVid();
          unsigned getViews();
          string getId();
          void setViews(unsigned num);
          void setId(string);

 };

